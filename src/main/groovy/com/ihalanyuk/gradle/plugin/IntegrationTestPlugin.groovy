/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.ihalanyuk.gradle.plugin


import com.ihalanyuk.gradle.util.CodeCoverageUtils
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.gradle.language.base.plugins.LifecycleBasePlugin
import org.gradle.plugins.ide.idea.IdeaPlugin
import org.gradle.testing.jacoco.plugins.JacocoPlugin
import org.gradle.testing.jacoco.tasks.JacocoReport

class IntegrationTestPluginExtension {
    String integrationTestName = 'integrationTest'
    String integrationTestDir = integrationTestName
    String javaSrcDir = "src/$integrationTestDir/java"
    String kotlinSrcDir = "src/$integrationTestDir/kotlin"
    String resourcesSrcDir = "src/$integrationTestDir/resources"
    String testResultDir = "test-results/$integrationTestDir/binary"
}

class IntegrationTestPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        def extension = project.extensions.create('itest', IntegrationTestPluginExtension)

        project.sourceSets {
            "$extension.integrationTestDir" {
                java.srcDir extension.javaSrcDir
                kotlin.srcDir extension.kotlinSrcDir
                resources.srcDir extension.resourcesSrcDir
            }
        }

        project.plugins.withType(IdeaPlugin) { // lazy as plugin not applied yet
            project.idea {
                module {
                    testSourceDirs += project.sourceSets."$extension.integrationTestDir".kotlin.srcDirs
                    testSourceDirs += project.sourceSets."$extension.integrationTestDir".resources.srcDirs
//                    scopes.TEST.plus.add(configurations.itCompile)
//                    scopes.TEST.plus.add(configurations.itRuntime)
                }
            }
        }

        project.dependencies {
            // Integration Tests source set dependencies
            // Provides, at compile-time, the classes produced by the main and test SourceSets
            integrationTestCompile project.sourceSets.main.output
            integrationTestCompile project.sourceSets.test.output
            // Provides, at compile-time, the dependencies that both main and test require
            // in order to successfully compile.
            integrationTestCompile project.configurations.implementation
            integrationTestCompile project.configurations.testImplementation
            // Provides, at run-time, the dependencies that both main and test require to run.
            integrationTestRuntime project.configurations.runtime
            integrationTestRuntime project.configurations.testRuntime
        }

        project.task(extension.integrationTestName, type: Test) {
            group = LifecycleBasePlugin.VERIFICATION_GROUP
            description = "Runs the ${extension.integrationTestName}."

            maxHeapSize = '1024m'

            testClassesDirs = project.sourceSets.integrationTest.output.classesDirs
            classpath = project.sourceSets.integrationTest.runtimeClasspath

            binResultsDir = new File(project.buildDir, extension.testResultDir)

            mustRunAfter project.tasks.test
        }

        project.plugins.withType(JacocoPlugin) {
            def jacocoReportTaskName = "jacoco${extension.integrationTestName.capitalize()}Report"
            project.task(jacocoReportTaskName, type: JacocoReport) {
                group = LifecycleBasePlugin.VERIFICATION_GROUP
                description = "Generates code coverage report for the ${extension.integrationTestName} task"

                executionData project.tasks.getByName(extension.integrationTestName)
                sourceSets project.sourceSets.main
                reports {
                    xml.enabled = true
                    csv.enabled = false
                    html.enabled = true
                }

                doLast {
                    def xmlReport = project.tasks.getByName(jacocoReportTaskName).reports.xml.destination
                    def coverage = CodeCoverageUtils.extractCodeCoverageFromXmlReport(xmlReport)
                    println("Total Coverage: " + (100 * coverage).round() + "%")
                }
            }
        }
    }
}
