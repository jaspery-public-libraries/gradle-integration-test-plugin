/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.ihalanyuk.gradle.util

class CodeCoverageUtils {
    private CodeCoverageUtils() {

    }

    static double extractCodeCoverageFromXmlReport(File file) {
        def parser = new XmlSlurper(false, true, true)
        parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)
        parser.setFeature("http://xml.org/sax/features/namespaces", false)

        def parsedReport = parser.parse(file)
        def inst = parsedReport.counter.find {
            c -> c.@type = 'INSTRUCTION'
        }

        def missed = inst.@missed.toInteger()
        def covered = inst.@covered.toInteger()

        return (covered / (covered + missed)).toDouble()
    }
}